[versions]
# common
core-ktx = "1.12.0"
coroutine = "1.7.2"
material = "1.9.0"
lifecycle-ktx = "2.6.2"

# di
hilt = "2.44"
hilt-navigation-compose = "1.0.0"

# compose
activity-compose = "1.7.2"
glide-compose = "1.0.0-alpha.5"
compose-bom = "2023.09.01"
compose-runtime-livedata = "1.5.1"
foundation-compose = "1.5.1"
navigation-compose = "2.7.3"

# network
retrofit = "2.9.0"
logging-interceptor = "4.11.0"

# db
room-db = "2.6.0-rc01"

# pagination
paging = "3.2.1"
paging-compose = "3.3.0-alpha02"

# test
junit = "4.13.2"
androidx-junit = "1.1.5"
espresso-core = "3.5.1"

[libraries]
# common
android-core-ktx = { module = "androidx.core:core-ktx", version.ref = "core-ktx" }

# lifecycle
android-lifecycle-runtime-ktx = { module = "androidx.lifecycle:lifecycle-runtime-ktx", version.ref = "lifecycle-ktx" }
android-lifecycle-livedata-ktx = { module = "androidx.lifecycle:lifecycle-livedata-ktx", version.ref = "lifecycle-ktx" }
android-lifecycle-viewmodel-ktx = { module = "androidx.lifecycle:lifecycle-viewmodel-ktx", version.ref = "lifecycle-ktx" }
android-lifecycle-viewmodel-compose = { module = "androidx.lifecycle:lifecycle-viewmodel-compose", version.ref = "lifecycle-ktx" }

# compose
android-activity-compose = { module = "androidx.activity:activity-compose", version.ref = "activity-compose" }
android-navigation-compose = { module = "androidx.navigation:navigation-compose", version.ref = "navigation-compose" }
android-foundation-compose = { module = "androidx.compose.foundation:foundation", version.ref = "foundation-compose" }
glide-compose = { module = "com.github.bumptech.glide:compose", version.ref = "glide-compose" }

# di
hilt-android = { module = "com.google.dagger:hilt-android", version.ref = "hilt" }
hilt-compiler = { module = "com.google.dagger:hilt-android-compiler", version.ref = "hilt" }
hilt-navigation-compose = { module = "androidx.hilt:hilt-navigation-compose", version.ref = "hilt-navigation-compose" }

# compose
compose-bom = { module = "androidx.compose:compose-bom", version.ref = "compose-bom" }
compose-ui = { module = "androidx.compose.ui:ui" }
compose-ui-graphics = { module = "androidx.compose.ui:ui-graphics" }
compose-ui-preview = { module = "androidx.compose.ui:ui-tooling-preview" }
compose-material3 = { module = "androidx.compose.material3:material3" }
compose-runtime-livedata = { module = "androidx.compose.runtime:runtime-livedata", version.ref = "compose-runtime-livedata" }

# network
retrofit-gson = { module = "com.squareup.retrofit2:converter-gson", version.ref = "retrofit" }
retrofit-interceptor-log = { module = "com.squareup.okhttp3:logging-interceptor", version.ref = "logging-interceptor" }
retrofit = { module = "com.squareup.retrofit2:retrofit", version.ref = "retrofit" }

# database
room-runtime = { module = "androidx.room:room-runtime", version.ref = "room-db" }
room-compiler = { module = "androidx.room:room-compiler", version.ref = "room-db" }
room-ktx = { module = "androidx.room:room-ktx", version.ref = "room-db" }

# paging
paging-runtime = { module = "androidx.paging:paging-runtime", version.ref = "paging" }
paging-compose = { module = "androidx.paging:paging-compose", version.ref = "paging-compose" }

# compose-test
compose-ui-test = { module = "androidx.compose.ui:ui-test-junit4" }
compose-ui-test-tooling = { module = "androidx.compose.ui:ui-tooling" }
compose-ui-test-manifest = { module = "androidx.compose.ui:ui-test-manifest" }

# test
junit = { module = "junit:junit", version.ref = "junit" }
android-junit = { module = "androidx.test.ext:junit", version.ref = "androidx-junit" }
android-espresso-core = { module = "androidx.test.espresso:espresso-core", version.ref = "espresso-core" }

[bundles]
compose = ["compose-ui", "compose-ui-graphics", "compose-ui-preview", "compose-material3"]
android-test = ["android-junit", "android-espresso-core"]
lifecycle = ["android-lifecycle-livedata-ktx", "android-lifecycle-viewmodel-compose", "android-lifecycle-viewmodel-ktx", "android-lifecycle-runtime-ktx"]
paging = ["paging-runtime", 'paging-compose']

[plugins]