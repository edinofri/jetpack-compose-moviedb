plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("kotlin-kapt")
    id("kotlin-parcelize")
    id("com.google.dagger.hilt.android")
}
android {
    namespace = "com.karizal.moviedb"
    compileSdk = 34

    defaultConfig {
        applicationId = "com.karizal.moviedb"
        minSdk = 21
        targetSdk = 34
        versionCode = 1
        versionName = "1.0.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }
    kotlinOptions {
        jvmTarget = "17"
    }
    buildFeatures {
        compose = true
        buildConfig = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = "1.4.3"
    }
    packaging {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
    buildTypes.forEach {
        it.buildConfigField(
            "String",
            "MOVIE_DB_ENDPOINT",
            "\"https://api.themoviedb.org/3/\""
        )
        it.buildConfigField(
            "String",
            "MOVIE_DB_TOKEN",
            "\"eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiJiN2NmNGJhNDIyNjQxNjhjZDA0OGUxYzVmZGE2OTBmMyIsInN1YiI6IjU5MzY0NzIyOTI1MTQxNmMwNTAwNzgyZiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.RSrZWIZ9Vi7o5pGnYscwi51UiJdT7Ub6j4lS2hmYlJQ\""
        )
        it.buildConfigField(
            "String",
            "MOVIE_IMAGE_ENDPOINT",
            "\"http://image.tmdb.org/t/p/w500\""
        )
    }
}

dependencies {
    // kotlin
    implementation(libs.android.core.ktx)
    // lifecycle
    implementation(libs.bundles.lifecycle)

    // compose
    implementation(libs.android.activity.compose)
    implementation(libs.android.navigation.compose)
    implementation(libs.android.foundation.compose)
    implementation(platform(libs.compose.bom))
    implementation(libs.bundles.compose)
    implementation(libs.compose.runtime.livedata)

    // glide
    implementation(libs.glide.compose)

    // hilt
    implementation(libs.hilt.android)
    implementation(libs.hilt.navigation.compose)
    kapt(libs.hilt.compiler)

    // network
    implementation(libs.retrofit)
    implementation(libs.retrofit.gson)
    implementation(libs.retrofit.interceptor.log)

    // database
    implementation(libs.room.runtime)
    implementation(libs.room.ktx)
    annotationProcessor(libs.room.compiler)
    kapt(libs.room.compiler)

    // paging
    implementation(libs.bundles.paging)

    // test
    testImplementation(libs.junit)
    androidTestImplementation(libs.bundles.android.test)
    androidTestImplementation(platform(libs.compose.bom))
    androidTestImplementation(libs.compose.ui.test)
    debugImplementation(libs.compose.ui.test.tooling)
    debugImplementation(libs.compose.ui.test.manifest)
}


// Allow references to generated code
kapt {
    correctErrorTypes = true
}