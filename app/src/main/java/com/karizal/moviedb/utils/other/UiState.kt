package com.karizal.moviedb.utils.other

import java.lang.Exception

sealed class UiState<T> {
    class Success<T>(val data: T) : UiState<T>()
    class Loading<T>(val isLoading: Boolean) : UiState<T>()
    class Error<T>(val exception: Exception) : UiState<T>()
}