package com.karizal.moviedb.utils.interceptor

import com.karizal.moviedb.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor: Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request().newBuilder().also {
            it.addHeader("Authorization:","Bearer ${BuildConfig.MOVIE_DB_TOKEN}")
        }

        return chain.proceed(request.build())
    }
}