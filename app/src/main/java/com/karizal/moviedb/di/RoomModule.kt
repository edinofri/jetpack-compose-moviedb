package com.karizal.moviedb.di

import android.content.Context
import androidx.room.Room
import com.karizal.moviedb.BuildConfig
import com.karizal.moviedb.data.source.local.AppDatabase
import com.karizal.moviedb.data.source.local.dao.GenreDao
import com.karizal.moviedb.data.source.local.dao.GenreDaoImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {
    @Singleton
    @Provides
    fun provideDatabase(
        @ApplicationContext context: Context
    ): AppDatabase {
        return Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "${BuildConfig.APPLICATION_ID}.db"
        ).fallbackToDestructiveMigration().build()
    }
    @Singleton
    @Provides
    fun provideGenreDao(
        appDatabase: AppDatabase
    ): GenreDao {
        return GenreDaoImpl(appDatabase.genreDao())
    }
}