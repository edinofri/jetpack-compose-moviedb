package com.karizal.moviedb.di

import com.karizal.moviedb.data.repo.GenreRepo
import com.karizal.moviedb.data.repo.MovieRepo
import com.karizal.moviedb.domain.usecase.GetGenreByIdsUseCase
import com.karizal.moviedb.domain.usecase.GetGenreUseCase
import com.karizal.moviedb.domain.usecase.GetMovieReviewUseCase
import com.karizal.moviedb.domain.usecase.GetMovieUseCase
import com.karizal.moviedb.domain.usecase.GetMovieVideosUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
object UseCaseModule {
    @Provides
    @Singleton
    fun provideGetGenreUseCase(
        genreRepo: GenreRepo
    ): GetGenreUseCase = GetGenreUseCase(genreRepo)

    @Provides
    @Singleton
    fun provideGetGenreByIdsUseCase(
        genreRepo: GenreRepo
    ): GetGenreByIdsUseCase = GetGenreByIdsUseCase(genreRepo)

    @Provides
    @Singleton
    fun provideGetMovieVideosUseCase(
        movieRepo: MovieRepo
    ): GetMovieVideosUseCase = GetMovieVideosUseCase(movieRepo)

    @Provides
    @Singleton
    fun provideGetMovieUseCase(
        movieRepo: MovieRepo
    ): GetMovieUseCase = GetMovieUseCase(movieRepo)

    @Provides
    @Singleton
    fun provideGetMovieReviewUseCase(
        movieRepo: MovieRepo
    ): GetMovieReviewUseCase = GetMovieReviewUseCase(movieRepo)
}