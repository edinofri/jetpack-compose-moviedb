package com.karizal.moviedb.di

import com.karizal.moviedb.data.repo.GenreRepo
import com.karizal.moviedb.data.repo.GenreRepoImpl
import com.karizal.moviedb.data.repo.MovieRepo
import com.karizal.moviedb.data.repo.MovieRepoImpl
import com.karizal.moviedb.data.source.local.dao.GenreDao
import com.karizal.moviedb.data.source.remote.service.MovieDBService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RepoModule {
    @Provides
    @Singleton
    fun provideGenreRepo(
        service: MovieDBService,
        genreDao: GenreDao,
    ): GenreRepo = GenreRepoImpl(service, genreDao)

    @Provides
    @Singleton
    fun provideMovieRepo(
        service: MovieDBService
    ): MovieRepo = MovieRepoImpl(service)
}