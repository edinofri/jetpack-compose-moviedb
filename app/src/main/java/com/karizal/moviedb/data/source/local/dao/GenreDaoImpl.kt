package com.karizal.moviedb.data.source.local.dao

import com.karizal.moviedb.data.source.local.room_dao.GenreRoomDao
import com.karizal.moviedb.domain.entity.GenreEntity
import javax.inject.Inject

class GenreDaoImpl @Inject constructor(
    private val genreRoomDao: GenreRoomDao
) : GenreDao {
    override suspend fun getGenres(): List<GenreEntity> {
        return genreRoomDao.get().map { it.toEntity() }
    }
    override suspend fun insertAll(genres: List<GenreEntity>) {
        deleteAll()
        genreRoomDao.insertAll(genres.map { it.toModel() })
    }
    override suspend fun findIds(ids: List<Int>): List<GenreEntity> {
        return genreRoomDao.findByIds(ids = ids).map { it.toEntity() }
    }
    override suspend fun deleteAll() {
        genreRoomDao.deleteAll()
    }
}