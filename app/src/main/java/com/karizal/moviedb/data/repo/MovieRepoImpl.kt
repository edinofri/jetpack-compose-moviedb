package com.karizal.moviedb.data.repo

import com.karizal.moviedb.data.source.remote.service.MovieDBService
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.domain.entity.ReviewEntity
import com.karizal.moviedb.domain.entity.VideoEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class MovieRepoImpl @Inject constructor(
    private val service: MovieDBService
) : MovieRepo {
    override suspend fun getMovies(
        page: Int,
        genreId: Int
    ): Flow<UiState<PaginationEntity<MovieEntity>>> {
        return flow {
            try {
                emit(UiState.Loading(true))
                val queries = hashMapOf<String, String>(
                    "page" to "$page",
                    "with_genres" to "$genreId"
                )
                val response = service.getMovies(queries)
                val result = response.toEntity { items ->
                    items.map {
                        it.toEntity()
                    }
                }
                emit(UiState.Loading(false))
                emit(UiState.Success(result))
            } catch (e: Exception) {
                emit(UiState.Loading(false))
                emit(UiState.Error(e))
            }
        }
    }

    override suspend fun getMovieVideos(movieId: Int): Flow<UiState<List<VideoEntity>>> {
        return flow {
            try {
                emit(UiState.Loading(true))
                val response = service.getMovieVideos(movieId)
                val result = response.results
                    .filter { it.site == "YouTube" }
                    .map { it.toEntity() }
                emit(UiState.Loading(false))
                emit(UiState.Success(result))
            } catch (e: Exception) {
                emit(UiState.Loading(false))
                emit(UiState.Error(e))
            }
        }
    }

    override suspend fun getMovieReviews(
        page: Int,
        movieId: Int
    ): Flow<UiState<PaginationEntity<ReviewEntity>>> {
        return flow {
            try {
                emit(UiState.Loading(true))
                val queries = hashMapOf<String, String>(
                    "page" to "$page",
                )
                val response = service.getMovieReviews(movieId, queries)
                val result = response.toEntity { items ->
                    items.map {
                        it.toEntity()
                    }
                }
                emit(UiState.Loading(false))
                emit(UiState.Success(result))
            } catch (e: Exception) {
                emit(UiState.Loading(false))
                emit(UiState.Error(e))
            }
        }
    }
}