package com.karizal.moviedb.data.repo

import com.karizal.moviedb.data.source.local.dao.GenreDao
import com.karizal.moviedb.data.source.remote.service.MovieDBService
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GenreRepoImpl @Inject constructor(
    private val service: MovieDBService,
    private val genreDao: GenreDao,
) : GenreRepo {
    override suspend fun getGenre(): Flow<UiState<List<GenreEntity>>> {
        return flow {
            try {
                emit(UiState.Loading(isLoading = true))
                val response = service.getCategories()
                val result = response.genres.map { it.toEntity() }
                if (result.isNotEmpty()) {
                    genreDao.insertAll(result)
                }
                emit(UiState.Loading(isLoading = false))
                emit(UiState.Success(result))
            } catch (e: Exception) {
                emit(UiState.Loading(isLoading = false))
                emit(UiState.Error(e))
            }
        }
    }

    override suspend fun getGenreByIds(ids: List<Int>): Flow<UiState<List<GenreEntity>>> {
        return flow {
            try {
                emit(UiState.Loading(true))
                val result = genreDao.findIds(ids)
                emit(UiState.Loading(false))
                emit(UiState.Success(result))
            } catch (e: Exception) {
                emit(UiState.Loading(false))
                emit(UiState.Error(e))
            }
        }
    }
}