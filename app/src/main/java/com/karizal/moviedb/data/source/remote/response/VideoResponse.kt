package com.karizal.moviedb.data.source.remote.response

import com.karizal.moviedb.domain.entity.VideoEntity

data class VideoResponse(
    val id: Int,
    val results: List<Video>
) {
    data class Video(
        val id: String,
        val type: String,
        val key: String,
        val site: String,
        val name: String
    ) {
        fun toEntity() = VideoEntity(id, type, key, site, name)
    }
}