package com.karizal.moviedb.data.source.local.room_dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.karizal.moviedb.data.source.local.model.GenreModel

@Dao
interface GenreRoomDao {
    @Query("SELECT * FROM genres")
    suspend fun get(): List<GenreModel>
    @Query("SELECT * FROM genres WHERE genre_id IN (:ids)")
    suspend fun findByIds(ids: List<Int>): List<GenreModel>
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(genreModel: List<GenreModel>)
    @Query("DELETE FROM genres WHERE 1=1")
    suspend fun deleteAll()
}