package com.karizal.moviedb.data.source.remote.response

import com.google.gson.annotations.SerializedName
import com.karizal.moviedb.domain.entity.ReviewEntity

data class ReviewResponse(
    val author: String,
    val content: String,
    @SerializedName("author_details")
    val authorDetails: ReviewDetail?,
    @SerializedName("created_at")
    val createdAt: String,
    @SerializedName("updated_at")
    val updatedAt: String
) {
    data class ReviewDetail(
        val rating: Float
    )
    fun toEntity() = ReviewEntity(author, content, authorDetails?.rating, createdAt, updatedAt)
}