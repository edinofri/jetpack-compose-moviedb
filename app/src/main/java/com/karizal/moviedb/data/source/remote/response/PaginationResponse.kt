package com.karizal.moviedb.data.source.remote.response

import com.google.gson.annotations.SerializedName
import com.karizal.moviedb.domain.entity.PaginationEntity

data class PaginationResponse<T>(
    val page: Int,
    val results: List<T>,
    @SerializedName("total_pages")
    val totalPerPages: Int,
    @SerializedName("total_results")
    val totalResults: Int,
) {
    fun <E> toEntity(mapItems: (List<T>) -> List<E>) =
        PaginationEntity(
            total = totalResults,
            currentPage = page,
            items = mapItems.invoke(results)
        )
}