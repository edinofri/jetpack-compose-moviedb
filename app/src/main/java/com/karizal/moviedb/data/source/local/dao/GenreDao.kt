package com.karizal.moviedb.data.source.local.dao

import com.karizal.moviedb.domain.entity.GenreEntity

interface GenreDao {
    suspend fun getGenres(): List<GenreEntity>
    suspend fun insertAll(genres: List<GenreEntity>)
    suspend fun findIds(ids: List<Int>): List<GenreEntity>
    suspend fun deleteAll()
}