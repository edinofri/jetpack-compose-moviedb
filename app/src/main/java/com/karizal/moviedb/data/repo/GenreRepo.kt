package com.karizal.moviedb.data.repo

import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow

interface GenreRepo {
    suspend fun getGenre(): Flow<UiState<List<GenreEntity>>>
    suspend fun getGenreByIds(ids: List<Int>): Flow<UiState<List<GenreEntity>>>
}