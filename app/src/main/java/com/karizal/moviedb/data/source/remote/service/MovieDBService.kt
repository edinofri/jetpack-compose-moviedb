package com.karizal.moviedb.data.source.remote.service

import com.karizal.moviedb.data.source.remote.response.GenreResponse
import com.karizal.moviedb.data.source.remote.response.MovieResponse
import com.karizal.moviedb.data.source.remote.response.PaginationResponse
import com.karizal.moviedb.data.source.remote.response.ReviewResponse
import com.karizal.moviedb.data.source.remote.response.VideoResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.QueryMap

interface MovieDBService {
    @GET("genre/movie/list")
    suspend fun getCategories(): GenreResponse

    @GET("discover/movie")
    suspend fun getMovies(@QueryMap queries: Map<String, String>): PaginationResponse<MovieResponse>

    @GET("movie/{movie_id}/videos?language=en-US")
    suspend fun getMovieVideos(@Path("movie_id") movieId: Int): VideoResponse

    @GET("movie/{movie_id}/reviews")
    suspend fun getMovieReviews(
        @Path("movie_id") movieId: Int, @QueryMap queries: Map<String, String>
    ): PaginationResponse<ReviewResponse>
}