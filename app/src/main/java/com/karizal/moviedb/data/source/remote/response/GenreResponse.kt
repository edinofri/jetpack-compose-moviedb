package com.karizal.moviedb.data.source.remote.response

import com.karizal.moviedb.domain.entity.GenreEntity

data class GenreResponse(
    val genres: List<GenreResponse.Genre>
) {
    data class Genre(
        val id: Int,
        val name: String
    ) {
        fun toEntity() = GenreEntity(id, name)
    }
}