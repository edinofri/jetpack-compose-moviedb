package com.karizal.moviedb.data.source.local

import androidx.room.Database
import androidx.room.RoomDatabase
import com.karizal.moviedb.data.source.local.model.GenreModel
import com.karizal.moviedb.data.source.local.room_dao.GenreRoomDao

@Database(
    entities = [
        GenreModel::class,
    ],
    version = 1,
    exportSchema = false
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun genreDao(): GenreRoomDao
}
