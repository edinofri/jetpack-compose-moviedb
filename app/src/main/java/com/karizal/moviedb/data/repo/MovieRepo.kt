package com.karizal.moviedb.data.repo

import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.domain.entity.ReviewEntity
import com.karizal.moviedb.domain.entity.VideoEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

interface MovieRepo {
    suspend fun getMovies(
        page: Int,
        genreId: Int
    ): Flow<UiState<PaginationEntity<MovieEntity>>>

    suspend fun getMovieVideos(
        movieId:Int
    ):Flow<UiState<List<VideoEntity>>>

    suspend fun getMovieReviews(
        page: Int,
        movieId: Int
    ):Flow<UiState<PaginationEntity<ReviewEntity>>>
}