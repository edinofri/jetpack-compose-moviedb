package com.karizal.moviedb.data.source.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.karizal.moviedb.domain.entity.GenreEntity

@Entity(tableName = "genres")
class GenreModel(
    @PrimaryKey(autoGenerate = true) var id: Long,
    @ColumnInfo(name = "genre_id") var genreId: Int,
    @ColumnInfo(name = "name") var name: String
) {
    fun toEntity() = GenreEntity(genreId, name)
}