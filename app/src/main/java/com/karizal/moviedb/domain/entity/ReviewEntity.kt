package com.karizal.moviedb.domain.entity

data class ReviewEntity(
    val author: String,
    val content: String,
    val rating: Float?,
    val createdAt: String,
    val updatedAt: String
)