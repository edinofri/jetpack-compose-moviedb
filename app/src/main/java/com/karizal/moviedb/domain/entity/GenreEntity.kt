package com.karizal.moviedb.domain.entity

import com.karizal.moviedb.data.source.local.model.GenreModel

data class GenreEntity(
    val id: Int,
    val name: String
) {
    fun toModel() = GenreModel(0, id, name)
}