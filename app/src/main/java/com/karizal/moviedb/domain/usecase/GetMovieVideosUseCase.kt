package com.karizal.moviedb.domain.usecase

import com.karizal.moviedb.data.repo.MovieRepo
import com.karizal.moviedb.domain.entity.VideoEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieVideosUseCase @Inject constructor(
    private val repo: MovieRepo
) {
    suspend fun execute(movieId: Int): Flow<UiState<List<VideoEntity>>> {
        return repo.getMovieVideos(movieId)
    }
}