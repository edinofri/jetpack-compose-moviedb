package com.karizal.moviedb.domain.entity

data class VideoEntity(
    val id: String,
    val type: String,
    val key: String,
    val site: String,
    val name: String
) {
    fun getThumbnail() = "https://img.youtube.com/vi/${key}/default.jpg"
    fun getYoutubeURL()= "https://www.youtube.com/watch?v=$key"
}