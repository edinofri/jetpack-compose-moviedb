package com.karizal.moviedb.domain.entity

data class PaginationEntity<T>(
    val items: List<T>,
    val total: Int,
    val currentPage: Int,
)