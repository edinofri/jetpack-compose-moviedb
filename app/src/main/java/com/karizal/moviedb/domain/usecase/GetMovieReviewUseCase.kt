package com.karizal.moviedb.domain.usecase

import com.karizal.moviedb.data.repo.MovieRepo
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.domain.entity.ReviewEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieReviewUseCase @Inject constructor(
    private val repo: MovieRepo
) {
    suspend fun execute(page: Int, movieId: Int): Flow<UiState<PaginationEntity<ReviewEntity>>> {
        return repo.getMovieReviews(page, movieId)
    }
}