package com.karizal.moviedb.domain.entity

import android.os.Parcelable
import com.karizal.moviedb.BuildConfig
import kotlinx.parcelize.Parcelize

@Parcelize
data class MovieEntity(
    val adult: Boolean,
    val backdropPath: String?,
    val genreIds: List<Int>,
    val id: Int,
    val originalLanguage: String,
    val originalTitle: String,
    val overview: String,
    val popularity: Double,
    val posterPath: String?,
    val releaseDate: String,
    val title: String,
    val video: Boolean,
    val voteAverage: Float,
    val voteCount: Int,
): Parcelable {
    fun getPoster(): String {
        return "${BuildConfig.MOVIE_IMAGE_ENDPOINT}$posterPath"
    }
}