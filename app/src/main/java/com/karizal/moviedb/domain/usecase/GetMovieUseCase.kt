package com.karizal.moviedb.domain.usecase

import com.karizal.moviedb.data.repo.GenreRepo
import com.karizal.moviedb.data.repo.MovieRepo
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetMovieUseCase @Inject constructor(
    private val repo: MovieRepo
) {
    suspend fun execute(page: Int, genreId: Int): Flow<UiState<PaginationEntity<MovieEntity>>> {
        return repo.getMovies(page, genreId)
    }
}