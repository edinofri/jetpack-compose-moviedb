package com.karizal.moviedb.domain.usecase

import com.karizal.moviedb.data.repo.GenreRepo
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.utils.other.UiState
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class GetGenreUseCase @Inject constructor(
    private val repo: GenreRepo
) {
    suspend fun execute(): Flow<UiState<List<GenreEntity>>> {
        return repo.getGenre()
    }
}