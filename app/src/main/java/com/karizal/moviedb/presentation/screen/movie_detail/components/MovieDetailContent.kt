package com.karizal.moviedb.presentation.screen.movie_detail.components

import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.ExperimentalLayoutApi
import androidx.compose.foundation.layout.FlowRow
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.DateRange
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material3.Divider
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.core.os.bundleOf
import androidx.navigation.NavHostController
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.VideoEntity
import com.karizal.moviedb.presentation.screen.movie_detail.MOVIE_PARAM
import com.karizal.moviedb.ui.components.Rating
import com.karizal.moviedb.ui.route.Screen
import com.karizal.moviedb.utils.other.UiState

@OptIn(ExperimentalLayoutApi::class)
@Composable
fun MovieDetailContent(
    navHostController: NavHostController,
    movie: MovieEntity,
    genres: UiState<List<GenreEntity>>?,
    trailers: UiState<List<VideoEntity>>?,
) {
    Column(
        Modifier
            .padding(top = 580.dp)
            .fillMaxWidth()
            .clip(RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp))
            .background(Color.White, shape = RoundedCornerShape(topStart = 16.dp, topEnd = 16.dp))
            .padding(16.dp)
    ) {
        Text(
            modifier = Modifier.fillMaxWidth(),
            text = movie.title,
            fontSize = 32.sp,
            lineHeight = 34.sp,
            fontWeight = FontWeight.Bold,
            textAlign = TextAlign.Center
        )

        if (genres is UiState.Success) {
            FlowRow(
                modifier = Modifier
                    .padding(8.dp)
                    .fillMaxWidth(),
                horizontalArrangement = Arrangement.Center
            ) {
                genres.data.forEach { genre ->
                    Text(
                        text = genre.name,
                        modifier = Modifier
                            .padding(8.dp)
                            .clip(RoundedCornerShape(8.dp))
                            .background(
                                color = Color.LightGray,
                                shape = RoundedCornerShape(8.dp)
                            )
                            .padding(8.dp),
                        overflow = TextOverflow.Ellipsis,
                        maxLines = 1
                    )
                }
            }
        }

        Row(modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.SpaceBetween) {
            Rating(value = movie.voteAverage, max = 10, modifier = Modifier.clickable {
                val destination = navHostController.findDestination(Screen.MovieRating.route)
                if (destination != null){
                    navHostController.navigate(destination.id, bundleOf(
                        MOVIE_PARAM to movie
                    ))
                }
            })
            ReleaseDate(value = movie.releaseDate)
        }

        Divider(
            modifier = Modifier
                .padding(vertical = 16.dp)
                .alpha(0.5f)
        )
        trailers?.let {
            if (it is UiState.Success) {
                MovieDetailTrailer(it.data)
            }
        }
        Divider(
            modifier = Modifier
                .padding(vertical = 16.dp)
                .alpha(0.5f)
        )
        Text(text = movie.overview)
    }
}

@Composable
private fun ReleaseDate(value: String) {
    Row {
        Icon(
            Icons.Rounded.DateRange,
            "date",
            tint = Color.LightGray,
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(text = value)
    }
}