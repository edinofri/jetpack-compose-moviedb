package com.karizal.moviedb.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.domain.entity.ReviewEntity
import com.karizal.moviedb.domain.usecase.GetMovieReviewUseCase
import com.karizal.moviedb.utils.other.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    private val getMovieReviewUseCase: GetMovieReviewUseCase
) : ViewModel() {

    var isLaunched: Boolean = false
    val reviews = MutableLiveData<UiState<PaginationEntity<ReviewEntity>>>()
    private var page = 1
    var items = mutableListOf<ReviewEntity>()

    fun getReviews(movieId: Int) = viewModelScope.launch {
        isLaunched = true
        if (reviews.value is UiState.Loading) {
            val isLoading =
                (reviews.value as UiState.Loading<PaginationEntity<ReviewEntity>>).isLoading
            if (isLoading) {
                // avoid call multiple times when still loading pagination
                return@launch
            }
        }
        if (reviews.value is UiState.Success) {
            val pagination = (reviews.value as UiState.Success<PaginationEntity<ReviewEntity>>)
            items = pagination.data.items.toMutableList()
            page = pagination.data.currentPage + 1
        }
        getMovieReviewUseCase.execute(page, movieId).collect {
            if (it is UiState.Success) {
                if (it.data.items.isNotEmpty()) {
                    items.addAll(it.data.items)
                    page = it.data.currentPage
                }
                reviews.value = it
            } else {
                reviews.value = it
            }
        }
    }
}