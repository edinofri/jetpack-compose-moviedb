package com.karizal.moviedb.presentation.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import androidx.navigation.compose.rememberNavController
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.presentation.viewmodel.GenreViewModel
import com.karizal.moviedb.ui.route.Screen
import com.karizal.moviedb.utils.other.UiState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun GenreScreen(navHostController: NavHostController) {
    val genreVM: GenreViewModel = hiltViewModel()
    val genresUiState by genreVM.genres.observeAsState()

    LaunchedEffect(genreVM.isLaunched) {
        if (genreVM.isLaunched.not()){
            genreVM.getGenre()
        }
    }

    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(text = "MovieDB")
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimary,
                    actionIconContentColor = MaterialTheme.colorScheme.onSecondary
                ),
            )
        }
    ) {
        when (genresUiState) {
            is UiState.Success -> {
                val items = (genresUiState as UiState.Success<List<GenreEntity>>).data
                LazyColumn(
                    modifier = Modifier
                        .padding(it)
                ) {
                    items(items) { genre ->
                        Text(
                            text = genre.name,
                            modifier = Modifier
                                .fillMaxWidth()
                                .clickable {
                                    navHostController.navigate("${Screen.Movie.route}/${genre.id}/${genre.name}")
                                }
                                .padding(16.dp),
                        )
                    }
                }
            }

            is UiState.Error -> {
                val exception = (genresUiState as UiState.Error).exception
                Text(text = "Error : ${exception.message}")
            }

            is UiState.Loading -> {
                val isLoading = (genresUiState as UiState.Loading).isLoading
                if (isLoading) {
                    CircularProgressIndicator()
                }
            }

            else -> {}
        }
    }
}


@Preview(showBackground = true)
@Composable
fun GenreScreenPreview() {
    GenreScreen(rememberNavController())
}
