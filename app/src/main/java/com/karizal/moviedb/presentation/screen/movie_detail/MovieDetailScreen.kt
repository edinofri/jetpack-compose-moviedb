package com.karizal.moviedb.presentation.screen.movie_detail

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.ext.getParcelableCompat
import com.karizal.moviedb.presentation.screen.movie_detail.components.MovieDetailContent
import com.karizal.moviedb.presentation.viewmodel.MovieDetailViewModel

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MovieDetailScreen(navHostController: NavHostController) {
    val movie = navHostController.currentBackStackEntry?.arguments?.getParcelableCompat(
        MOVIE_PARAM,
        MovieEntity::class.java
    )
    val movieDetailVM: MovieDetailViewModel = hiltViewModel()
    val genres by movieDetailVM.genres.observeAsState()
    val trailers by movieDetailVM.trailerVideos.observeAsState()

    LaunchedEffect(movieDetailVM.isLaunched) {
        if (movieDetailVM.isLaunched.not() && movie != null) {
            movieDetailVM.getGenresAndTrailer(movie = movie)
        }
    }

    movie?.let {
        Box {
            GlideImage(
                movie.getPoster(),
                modifier = Modifier
                    .fillMaxWidth()
                    .height(600.dp),
                contentDescription = "poster",
                contentScale = ContentScale.Crop
            )
            LazyColumn(
                userScrollEnabled = true
            ) {
                item {
                    MovieDetailContent(
                        navHostController = navHostController,
                        movie = movie,
                        genres = genres,
                        trailers = trailers,
                    )
                }
            }
        }
    }
}

const val MOVIE_PARAM = "movie"