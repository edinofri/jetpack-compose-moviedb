package com.karizal.moviedb.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.domain.usecase.GetGenreUseCase
import com.karizal.moviedb.utils.other.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GenreViewModel @Inject constructor(
    private val getGenreUseCase: GetGenreUseCase
) : ViewModel() {
    var isLaunched: Boolean = false

    val genres = MutableLiveData<UiState<List<GenreEntity>>>()
    fun getGenre() = viewModelScope.launch {
        isLaunched = true
        getGenreUseCase.execute().collect {
            genres.value = it
        }
    }
}