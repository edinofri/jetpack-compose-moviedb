package com.karizal.moviedb.presentation.screen.movie_detail.components

import android.content.Intent
import android.net.Uri
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.core.content.ContextCompat.startActivity
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.karizal.moviedb.domain.entity.VideoEntity


@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MovieDetailTrailer(trailers: List<VideoEntity>) {
    val context = LocalContext.current
    Text("Trailers", fontWeight = FontWeight.Bold, modifier = Modifier.padding(bottom = 8.dp))
    LazyRow(modifier = Modifier.fillMaxWidth()) {
        items(trailers) { trailer ->
            GlideImage(
                trailer.getThumbnail(),
                contentDescription = "trailer",
                modifier = Modifier
                    .padding(end = 8.dp)
                    .width(200.dp)
                    .height(120.dp)
                    .clickable {
                        val intent = Intent(Intent.ACTION_VIEW, Uri.parse(trailer.getYoutubeURL()))
                        context.startActivity(intent)
                    },
                contentScale = ContentScale.Crop
            )
        }
    }
}