package com.karizal.moviedb.presentation.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.Divider
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.ReviewEntity
import com.karizal.moviedb.ext.getParcelableCompat
import com.karizal.moviedb.presentation.screen.movie_detail.MOVIE_PARAM
import com.karizal.moviedb.presentation.viewmodel.ReviewViewModel
import com.karizal.moviedb.ui.components.Rating
import com.karizal.moviedb.utils.other.UiState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun RatingScreen(navHostController: NavHostController) {
    val movie = navHostController.currentBackStackEntry?.arguments?.getParcelableCompat(
        MOVIE_PARAM,
        MovieEntity::class.java
    ) ?: return
    val reviewVM: ReviewViewModel = hiltViewModel()
    val reviewUiState by reviewVM.reviews.observeAsState()
    val listState = rememberLazyListState()

    val shouldStartPaginate = remember {
        derivedStateOf {
            (listState.layoutInfo.visibleItemsInfo.lastOrNull()?.index
                ?: -9) >= (listState.layoutInfo.totalItemsCount - 6)
        }
    }
    LaunchedEffect(reviewVM.isLaunched) {
        if (reviewVM.isLaunched.not()) {
            reviewVM.getReviews(movie.id)
        }
    }
    LaunchedEffect(shouldStartPaginate.value) {
        if (shouldStartPaginate.value && reviewUiState is UiState.Success) {
            reviewVM.getReviews(movie.id)
        }
    }
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(text = "Reviews of ${movie.title}", maxLines = 1)
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimary,
                    actionIconContentColor = MaterialTheme.colorScheme.onSecondary
                ),
            )
        }
    ) {
        Box() {
            when (reviewUiState) {
                is UiState.Success, is UiState.Loading -> {
                    LazyColumn(
                        modifier = Modifier
                            .padding(it)
                            .fillMaxWidth()
                            .padding(8.dp),
                        state = listState,
                    ) {
                        items(reviewVM.items) {
                            ReviewComponent(it)
                        }
                        if (reviewVM.items.isEmpty() && reviewUiState is UiState.Success) {
                            item {
                                Box(
                                    modifier = Modifier.fillMaxWidth(),
                                    contentAlignment = Alignment.Center
                                ) {
                                    Text(text = "No review for this movie yet.")
                                }
                            }
                        }
                        if (reviewUiState is UiState.Loading) {
                            val loadingItems = if (reviewVM.items.isEmpty()) {
                                4
                            } else {
                                2
                            }
                            items(loadingItems) {
                                val isLoading = (reviewUiState as UiState.Loading).isLoading
                                if (isLoading) {
                                    ReviewLoading()
                                }
                            }
                        }
                    }
                }

                is UiState.Error -> {
                    val exception = (reviewUiState as UiState.Error).exception
                    Text(text = "Error : ${exception.message}")
                }

                else -> {}
            }
        }
    }
}

@Composable
fun ReviewComponent(reviewEntity: ReviewEntity) {
    Column(
        Modifier.padding(horizontal = 16.dp, vertical = 8.dp)
    ) {
        Row(
            modifier = Modifier
                .padding(bottom = 16.dp)
                .fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceBetween
        ) {
            Text(
                text = reviewEntity.author.replaceFirstChar { it.uppercaseChar() },
                fontWeight = FontWeight.Bold
            )
            Rating(value = reviewEntity.rating, max = 10)
        }
        Text(text = reviewEntity.content)
        Divider(Modifier.padding(vertical = 16.dp))
    }
}

@Composable
fun ReviewLoading() {

}