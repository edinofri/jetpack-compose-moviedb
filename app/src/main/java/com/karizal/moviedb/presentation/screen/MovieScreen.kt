package com.karizal.moviedb.presentation.screen

import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.staggeredgrid.LazyVerticalStaggeredGrid
import androidx.compose.foundation.lazy.staggeredgrid.StaggeredGridCells
import androidx.compose.foundation.lazy.staggeredgrid.items
import androidx.compose.foundation.lazy.staggeredgrid.rememberLazyStaggeredGridState
import androidx.compose.material3.CenterAlignedTopAppBar
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.derivedStateOf
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.core.os.bundleOf
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavHostController
import com.karizal.moviedb.presentation.screen.movie_detail.MOVIE_PARAM
import com.karizal.moviedb.presentation.viewmodel.MovieViewModel
import com.karizal.moviedb.ui.components.MovieCard
import com.karizal.moviedb.ui.components.MovieLoadingCard
import com.karizal.moviedb.ui.route.Screen
import com.karizal.moviedb.utils.other.UiState

@OptIn(ExperimentalMaterial3Api::class)
@Composable
fun MovieScreen(navHostController: NavHostController, genreId: Int, genreName: String) {
    val movieVM: MovieViewModel = hiltViewModel()
    val moviesUiState by movieVM.movies.observeAsState()
    val staggeredState = rememberLazyStaggeredGridState()

    val shouldStartPaginate = remember {
        derivedStateOf {
            (staggeredState.layoutInfo.visibleItemsInfo.lastOrNull()?.index
                ?: -9) >= (staggeredState.layoutInfo.totalItemsCount - 4)
        }
    }
    LaunchedEffect(movieVM.isLaunched) {
        if (movieVM.isLaunched.not()) {
            movieVM.geMovies(genreId)
        }
    }
    LaunchedEffect(shouldStartPaginate.value) {
        if (shouldStartPaginate.value && moviesUiState is UiState.Success) {
            movieVM.geMovies(genreId)
        }
    }
    Scaffold(
        topBar = {
            CenterAlignedTopAppBar(
                title = {
                    Text(text = genreName)
                },
                colors = TopAppBarDefaults.topAppBarColors(
                    containerColor = MaterialTheme.colorScheme.primary,
                    titleContentColor = MaterialTheme.colorScheme.onPrimary,
                    navigationIconContentColor = MaterialTheme.colorScheme.onPrimary,
                    actionIconContentColor = MaterialTheme.colorScheme.onSecondary
                ),
            )
        }
    ) {
        Box() {
            when (moviesUiState) {
                is UiState.Success, is UiState.Loading -> {
                    LazyVerticalStaggeredGrid(
                        modifier = Modifier
                            .padding(it)
                            .fillMaxWidth()
                            .padding(8.dp),
                        state = staggeredState,
                        columns = StaggeredGridCells.Fixed(2),
                        verticalItemSpacing = 8.dp,
                        horizontalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        items(movieVM.items) { movie ->
                            MovieCard(
                                modifier = Modifier.clickable {
                                    val destination =
                                        navHostController.graph.findNode(Screen.MovieDetail.route)
                                    if (destination != null) {
                                        navHostController.navigate(
                                            destination.id,
                                            bundleOf(
                                                MOVIE_PARAM to movie,
                                            )
                                        )
                                    }
                                },
                                movieEntity = movie,
                            )
                        }
                        if (moviesUiState is UiState.Loading) {
                            val loadingItems = if (movieVM.items.isEmpty()) {
                                4
                            } else {
                                2
                            }
                            items(loadingItems) {
                                val isLoading = (moviesUiState as UiState.Loading).isLoading
                                if (isLoading) {
                                    MovieLoadingCard()
                                }
                            }
                        }
                    }
                }

                is UiState.Error -> {
                    val exception = (moviesUiState as UiState.Error).exception
                    Text(text = "Error : ${exception.message}")
                }

                else -> {}
            }
        }
    }
}