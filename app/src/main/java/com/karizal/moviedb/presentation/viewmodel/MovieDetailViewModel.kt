package com.karizal.moviedb.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karizal.moviedb.domain.entity.GenreEntity
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.VideoEntity
import com.karizal.moviedb.domain.usecase.GetGenreByIdsUseCase
import com.karizal.moviedb.domain.usecase.GetMovieVideosUseCase
import com.karizal.moviedb.utils.other.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieDetailViewModel @Inject constructor(
    private val getGenreByIdsUseCase: GetGenreByIdsUseCase,
    private val getMovieVideosUseCase: GetMovieVideosUseCase
) : ViewModel() {
    var isLaunched: Boolean = false

    val genres = MutableLiveData<UiState<List<GenreEntity>>>()
    val trailerVideos = MutableLiveData<UiState<List<VideoEntity>>>()

    fun getGenresAndTrailer(movie: MovieEntity) = viewModelScope.launch {
        getGenreByIdsUseCase.execute(movie.genreIds).collect {
            genres.value = it
        }

        getMovieVideosUseCase.execute(movie.id).collect {
            trailerVideos.value = it
        }
    }
}