package com.karizal.moviedb.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.karizal.moviedb.domain.entity.MovieEntity
import com.karizal.moviedb.domain.entity.PaginationEntity
import com.karizal.moviedb.domain.usecase.GetMovieUseCase
import com.karizal.moviedb.domain.usecase.GetMovieVideosUseCase
import com.karizal.moviedb.utils.other.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MovieViewModel @Inject constructor(
    private val getMovieUseCase: GetMovieUseCase,
) : ViewModel() {
    var isLaunched: Boolean = false
    val movies = MutableLiveData<UiState<PaginationEntity<MovieEntity>>>()
    private var page = 1
    var items = mutableListOf<MovieEntity>()
    fun geMovies(genreId: Int) = viewModelScope.launch {
        isLaunched = true
        if (movies.value is UiState.Loading) {
            val isLoading =
                (movies.value as UiState.Loading<PaginationEntity<MovieEntity>>).isLoading
            if (isLoading) {
                // avoid call multiple times when still loading pagination
                return@launch
            }
        }
        if (movies.value is UiState.Success) {
            val pagination = (movies.value as UiState.Success<PaginationEntity<MovieEntity>>)
            items = pagination.data.items.toMutableList()
            page = pagination.data.currentPage + 1
        }
        getMovieUseCase.execute(page, genreId).collect {
            if (it is UiState.Success) {
                if (it.data.items.isNotEmpty()) {
                    items.addAll(it.data.items)
                    page = it.data.currentPage
                }
                movies.value = it
            } else {
                movies.value = it
            }
        }
    }
}