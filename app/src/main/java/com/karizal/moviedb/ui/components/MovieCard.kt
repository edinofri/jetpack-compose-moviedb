package com.karizal.moviedb.ui.components

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.unit.dp
import com.bumptech.glide.integration.compose.ExperimentalGlideComposeApi
import com.bumptech.glide.integration.compose.GlideImage
import com.karizal.moviedb.domain.entity.MovieEntity

@OptIn(ExperimentalGlideComposeApi::class)
@Composable
fun MovieCard(
    modifier: Modifier = Modifier,
    movieEntity: MovieEntity,
) {
    Column {
        GlideImage(
            modifier = modifier
                .clip(RoundedCornerShape(8.dp))
                .fillMaxWidth()
                .height(320.dp),
            model = movieEntity.getPoster(),
            contentDescription = "Poster",
            contentScale = ContentScale.Crop
        )
    }
}