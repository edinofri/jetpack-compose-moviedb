package com.karizal.moviedb.ui.route

sealed class Screen(val route: String) {
    object Genre : Screen("genre_screen")
    object Movie : Screen("movie_screen")
    object MovieDetail : Screen("movie_detail_screen")
    object MovieRating : Screen("rating_screen")
}