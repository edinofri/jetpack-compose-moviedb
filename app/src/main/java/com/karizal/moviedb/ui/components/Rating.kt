package com.karizal.moviedb.ui.components

import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.Star
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun Rating(modifier: Modifier = Modifier, value: Float?, max: Int) {
    Row(modifier) {
        Icon(
            Icons.Rounded.Star,
            "star",
            tint = Color(255, 165, 0),
            modifier = Modifier.padding(end = 8.dp)
        )
        Text(text = "Rating ${value ?: "-"}/$max")
    }
}