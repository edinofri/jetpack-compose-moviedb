package com.karizal.moviedb.ui.route

import androidx.compose.runtime.Composable
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.karizal.moviedb.presentation.screen.GenreScreen
import com.karizal.moviedb.presentation.screen.movie_detail.MovieDetailScreen
import com.karizal.moviedb.presentation.screen.MovieScreen
import com.karizal.moviedb.presentation.screen.RatingScreen

object Route {
    @Composable
    fun Setup(navHostController: NavHostController) {
        NavHost(navController = navHostController, startDestination = Screen.Genre.route) {
            composable(
                route = Screen.Genre.route,
            ) {
                GenreScreen(navHostController)
            }
            composable(
                route = "${Screen.Movie.route}/{genre_id}/{genre_name}"
            ) { navBackStackEntry ->
                val genreId = navBackStackEntry.arguments?.getString("genre_id")
                val genreName = navBackStackEntry.arguments?.getString("genre_name")
                if (genreId != null && genreName != null) {
                    MovieScreen(navHostController, genreId = genreId.toInt(), genreName = genreName)
                }
            }
            composable(
                route = Screen.MovieDetail.route
            ) {
                MovieDetailScreen(navHostController)
            }
            composable(
                route = Screen.MovieRating.route
            ) {
                RatingScreen(navHostController)
            }
        }
    }
}